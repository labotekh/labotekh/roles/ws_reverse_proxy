# `labotekh.labotekh.ws_reverse_proxy`
The `labotekh.labotekh.ws_reverse_proxy` role configure the Nginx site for the each service and create the TLS/SSL certificate if the FQDN isn't a subdomain of the root domain of the host.

The service site serves URLS `/static/*`, `/media/*`, `/favicon.ico` as static files stored in the `files` directory.
