#!/bin/sh
cd {{ root_directory }}/{{ reverse_proxy_dirname }}/
docker-compose run --rm certbot certonly \
  -d {{ item.value.root_domain }} -m {{ certbot_email }} \
  --webroot --agree-tos -n
